<?php

namespace Tests;

use Orchestra\Testbench\TestCase;
use Tests\Models\Product;

/**
 * @coversNothing
 */
class GenerateCodeTest extends TestCase
{
    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->withFactories(__DIR__ . '/database/factories');
    }

    public function testCan_generate_code()
    {
        $p1 = factory(Product::class)->create();

        $p2 = factory(Product::class)->create();

        $p3 = factory(Product::class)->create();

        $this->assertSame($p1->code, 'p0001');

        $this->assertSame($p2->code, 'p0002');

        $this->assertSame($p3->code, 'p0003');
    }
}
