<?php

namespace Tests\Models;

use Illuminate\Database\Eloquent\Model;
use FutureSoft\LaravelBoilerplate\GenerateCode;

class Product extends Model
{
    use GenerateCode;

    protected $fillable = [
        'code',
        'name',
    ];

    public function codeDetails()
    {
        return [
            'prefix' => 'p',
            'column' => 'code',
        ];
    }
}
