<?php

// @var $factory \Illuminate\Database\Eloquent\Factory

use Faker\Generator as Faker;
use Tests\Models\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
