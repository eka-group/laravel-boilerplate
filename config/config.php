<?php

return [
    // Content to show when the sidebar is collapsed
    'logo' => '<b>G</b>C',

    // Asset path. Default: "bower_components"
    'theme' => 'bower_components',

    // The skin to use for the application
    'skin' => 'skin-blue',

    // View to use as header
    'header' => 'laravel-boilerplate::partials.header',

    // View to use as sidebar
    'sidebar' => 'laravel-boilerplate::partials.sidebar',
];
