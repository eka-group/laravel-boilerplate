<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme') }}/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme') }}/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme') }}/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    @stack('style')
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme') }}/admin-lte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme') }}/admin-lte/dist/css/skins/{{ config('laravel-boilerplate.skin') }}.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="@yield('body-class')">
    <div id="app">@yield('body-content')</div>

    <script src="/{{ config('laravel-boilerplate.theme') }}/jquery/dist/jquery.min.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme') }}/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{ mix('/js/app.js') }}"></script>
    @stack('script')
</body>
</html>
