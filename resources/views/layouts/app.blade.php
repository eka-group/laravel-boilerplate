@extends('laravel-boilerplate::layouts.master')

@section('body-class', 'hold-transition '. config('laravel-boilerplate.skin') .' sidebar-mini')

@section('body-content')
    <div class="wrapper">
        @include(config('laravel-boilerplate.header'))
        @include(config('laravel-boilerplate.sidebar'))

        <div class="content-wrapper">
            <section class="content-header">
                @yield('content-header')

                {{ Breadcrumbs::render() }}
            </section>

            <section class="content">
                @if(flash()->message)
                    <div class="{{ flash()->class }}">
                        @if (!is_array(flash()->message))
                            {!! flash()->message !!}
                        @else
                            <ul>
                                @foreach (flash()->message as $msg)
                                    <li>{!! $msg !!}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                @endif

                @yield('content')
            </section>
        </div>

        @include('laravel-boilerplate::partials.footer')
    </div>
@endsection

@push('script')
    <script src="/{{ config('laravel-boilerplate.theme') }}/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme') }}/fastclick/lib/fastclick.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme') }}/admin-lte/dist/js/adminlte.min.js"></script>
@endpush
