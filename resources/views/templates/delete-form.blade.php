<form id="delete-form" class="hidden" method="post">
    @csrf
    @method('delete')
</form>
