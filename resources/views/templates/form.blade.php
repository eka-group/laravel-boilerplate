{!! form_start($form) !!}
    <div class="box-body">
        @foreach ($form->getFields() as $name => $field)
            @if (!str_contains($name, ['submit', 'submit_new', 'reset']))
                {!! form_row($field) !!}
            @endif
        @endforeach
    </div>

    <div class="box-footer">
        @include('laravel-boilerplate::templates.form-footer-buttons', ['form' => $form])
    </div>
{!! form_end($form, false) !!}
