@extends('laravel-boilerplate::layouts.app')

@section('title', $form->description)

@section('content-header')
    <h1>
        {{ $form->title }}
        <small>{{ $form->description }}</small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @include('laravel-boilerplate::templates.form', ['form' => $form])
            </div>
        </div>
    </div>
@endsection
