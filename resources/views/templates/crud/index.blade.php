@extends('laravel-boilerplate::layouts.app')

@section('title', $title)

@push('style')
    @include('laravel-boilerplate::templates.datatable-css')
@endpush

@section('content-header')
    <h1>
        {{ $title }}
        <small>Display all {{ Str::lower($title) }}</small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>

    @include('laravel-boilerplate::templates.delete-form')
@endsection

@push('script')
    @include('laravel-boilerplate::templates.datatable-js')

    @include('laravel-boilerplate::templates.delete-record')
@endpush
