<script src="/{{ config('laravel-boilerplate.theme') }}/bootbox.js/dist/bootbox.all.min.js"></script>
<script>
    $(document).on('click', '.delete', function (e) {
        e.preventDefault();

        $('#delete-form').attr('action', $(this).attr('href'));

        const deleteLabel = $(this).data('delete-label') || 'Supprimer';

        bootbox.confirm({
            message: 'Etes vous sûr ?',
            buttons: {
                cancel: {
                    label: 'Annuler',
                    className: 'btn-sm'
                },
                confirm: {
                    label: `<i class="fa fa-trash-o"></i> ${deleteLabel}`,
                    className: 'btn-sm btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $('.bootbox-confirm button').attr('disabled', true);
                    $('.bootbox-confirm button[data-bb-handler="confirm"]').html(`<i class="fa fa-spin fa-spinner"></i> ${deleteLabel}`);

                    $('#delete-form').submit();
                }

                return !result;
            }
        });
    });
</script>
