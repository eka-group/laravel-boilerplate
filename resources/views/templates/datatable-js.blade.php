<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-autofill/js/dataTables.autoFill.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables-autofill-bs/js/autoFill.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-colreorder/js/dataTables.colReorder.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-colreorder-bs/js/colReorder.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-fixedcolumns-bs/js/fixedColumns.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-fixedheader-bs/js/fixedHeader.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-keytable-bs/js/keyTable.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-responsive-bs/js/responsive.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-rowgroup/js/dataTables.rowGroup.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-rowgroup-bs/js/rowGroup.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-rowreorder/js/dataTables.rowReorder.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-rowreorder-bs/js/rowReorder.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-scroller-bs/js/scroller.bootstrap.min.js"></script>

<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-select/js/dataTables.select.min.js"></script>
<script src="/{{ config('laravel-boilerplate.theme') }}/datatables.net-select-bs/js/select.bootstrap.min.js"></script>

<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
