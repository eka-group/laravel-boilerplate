@push('style')
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
@endpush

@push('script')
    <script src="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/{{ config('laravel-boilerplate.theme_path') }}/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush



