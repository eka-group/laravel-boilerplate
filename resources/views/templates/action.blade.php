@isset($print)
    <a href="{{ $print }}" target="_blank" class="btn btn-sm btn-default"><i class="fa fa fa-print"></i></a>
@endisset

@isset($details)
    <a href="{{ $details }}" class="btn btn-sm btn-default"><i class="fa fa fa-arrow-circle-o-right"></i></a>
@endisset

@isset($edit)
    <a href="{{ $edit }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
@endisset

@isset($delete)
    <a href="{{ $delete }}" class="btn btn-sm btn-danger delete"><i class="fa fa-trash-o"></i></a>
@endisset
