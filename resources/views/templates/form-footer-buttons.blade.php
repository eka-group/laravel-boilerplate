{!! form_row($form->submit) !!}

@if (array_has($form->getFields(), 'submit_new'))
{!! form_row($form->submit_new) !!}
@endif


@if (array_has($form->getFields(), 'reset'))
{!! form_row($form->reset) !!}
@endif

<div class="pull-right">
    <a href="{{ $form->back() }}" class="btn btn-default">Cancel</a>
</div>
