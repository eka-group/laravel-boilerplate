<?php

namespace FutureSoft\LaravelBoilerplate;

use Illuminate\Support\Str;
use Yajra\DataTables\Services\DataTable as BaseDataTable;

class DataTable extends BaseDataTable
{
    /**
     * Action view to use.
     *
     * @var string
     */
    protected $actionViewName;

    /**
     * Action column width.
     *
     * @var string
     */
    protected $actionColumnwidth = '200px';

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $datatable = datatables($query);

        if (null !== $this->actionViewName) {
            $datatable = $datatable->addColumn('action', function ($model) {
                return view($this->actionViewName, compact('model'));
            });
        }

        return $datatable;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $builder = $this->builder()
            ->columns($this->getColumns())
            ->addCheckbox([], true)
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());

        if (null !== $this->actionViewName) {
            $builder = $builder->addAction(['width' => $this->actionColumnwidth]);
        }

        return $builder;
    }

    /**
     * Export to PDF.
     *
     * @return mixed
     */
    public function exportToPdf()
    {
        echo 'Export to pdf';
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return array_merge_recursive(config('datatables-buttons.parameters'), [
            'order' => [[1, 'desc']],
        ]);
        // default parameters
        // 'dom'     => 'Bfrtip',
        // 'order'   => [[0, 'desc']],
        // 'buttons' => [
        //     'create',
        //     'export',
        //     'print',
        //     'reset',
        //     'reload',
        // ],

        // add custom buttons
        // return array_merge(config('datatables-buttons.parameters'), [
        //     // 'buttons' => ['exportToPdf', 'reset', 'reload'],
        //     'buttons' => ['reset', 'reload'],
        //     'iDisplayLength' => 25,
        //     'lengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'All']],
        // ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return Str::random(32);
    }
}
