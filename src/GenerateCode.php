<?php

namespace FutureSoft\LaravelBoilerplate;

use Illuminate\Support\Str;

trait GenerateCode
{
    /**
     * The "booting" method of the trait.
     *
     * @return void
     */
    public static function bootGenerateCode()
    {
        static::creating(function ($model) {
            $details = $model->codeDetails();

            $model->code = $model->generate(
                $details['prefix'] ?? '',
                $details['column'] ?? 'code'
            );
        });
    }

    /**
     * Get settings for generating code.
     *
     * @return array
     */
    abstract public function codeDetails(): array;

    /**
     * Generate code.
     *
     * @param string $prefix
     * @param string $column
     * @return string
     */
    protected function generate($prefix, $column)
    {
        $code = optional($this->newQuery()
            ->where($column, 'like', $prefix . '%')
            ->orderByDesc($column)
            ->first([$column]))
            ->{$column};

        $code = blank($code) ? 0 : $code;

        $currentIndex = (int) trim(Str::after($code, $prefix));

        $nextIndex = $currentIndex + 1;

        $nextSuffix = str_pad($nextIndex, 4, '0', STR_PAD_LEFT);

        $codeGenerated = $prefix . $nextSuffix;

        return $codeGenerated;
    }
}
