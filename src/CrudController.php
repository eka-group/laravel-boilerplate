<?php

namespace FutureSoft\LaravelBoilerplate;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class CrudController extends BaseController
{
    use AuthorizesRequests,
    DispatchesJobs,
    FormBuilderTrait,
    ValidatesRequests;

    /**
     * Model class to use
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Form class to use
     *
     * @var \FutureSoft\LaravelBoilerplate\Form
     */
    protected $form;

    /**
     * DataTable class to use
     *
     * @var \FutureSoft\LaravelBoilerplate\DataTable
     */
    protected $dataTable;

    /**
     * View sub folder to use
     *
     * @var string
     */
    protected $viewPrefix;

    /**
     * Route prefix
     *
     * @var string
     */
    protected $routePrefix;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new $this->model;

        $this->dataTable = new $this->dataTable;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->dataTable->render($this->viewPrefix . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = $this->form($this->form);

        return view($this->viewPrefix . '.form', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->form($this->form)->redirectIfNotValid();

        $this->model->save($request->all());

        $redirect = $request->has('submit_new')
            ? redirect()->back()
            : redirect()->route($this->routePrefix . '.index');

        flash()->success(class_basename($this->model) . ' was created successfully!');

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $model
     * @return \Illuminate\Http\Response
     */
    public function show($model)
    {
        $name = Str::camel(class_basename($this->model));

        return view($this->viewPrefix . '.show')->with($name, $this->model->findOrFail($model));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($model)
    {
        $form = $this->form($this->form, ['model' => $this->model->findOrFail($model)]);

        $form->remove('submit_new');

        return view($this->viewPrefix . '.form', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $model)
    {
        $model = $this->model->findOrFail($model);

        $this->form($this->form, ['model' => $model])->redirectIfNotValid();

        $model->update($request->all());

        flash()->success(class_basename($this->model) . ' was updated successfully!');

        return redirect()->route($this->routePrefix . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $model)
    {
        if ($this->model->findOrFail($model)->delete()) {
            flash()->success(class_basename($this->model) . ' was deleted successfully!');
        } else {
            flash()->error('An error occured, try again!');
        }

        return redirect()->route($this->routePrefix . '.index');
    }
}
