<?php

namespace FutureSoft\LaravelBoilerplate;

use Illuminate\Support\ServiceProvider;

class BoilerplateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'laravel-boilerplate');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // view, route, etc. namespace
        $namespace = 'laravel-boilerplate';

        // register package's views
        $this->loadViewsFrom(__DIR__ . '/../resources/views', $namespace);

        $this->publishAssets($namespace);

        $this->commands(CrudGenerator::class);
    }

    /**
     * Publish datatables assets.
     *
     * @param string $namespace
     * @return void
     */
    private function publishAssets($namespace)
    {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('laravel-boilerplate.php'),
        ], $namespace . '-config');

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/' . $namespace),
        ], $namespace . '-views');
    }
}
