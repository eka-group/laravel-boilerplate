<?php

namespace FutureSoft\LaravelBoilerplate;

use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Form as BaseForm;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class Form extends BaseForm
{
    /**
     * Storing or Updating data.
     *
     * @var bool
     */
    public $updating = false;

    /**
     * Http method to use for posting data to server.
     *
     * @var string
     */
    protected $submitMethodName = 'POST';

    /**
     * Http method to use for posting data to server when updating.
     *
     * @var string
     */
    protected $updateMethodName = 'PUT';

    /**
     * Build the form.
     *
     * @return mixed
     */
    public function buildForm()
    {
        $this->updating = $this->getModel() !== [];

        $method = null !== $this->updateMethodName && $this->updating
            ? $this->updateMethodName
            : $this->submitMethodName;

        $this->setFormOptions([
            'method' => $method,
            'autocomplete' => 'off',
            'url' => $this->updating ? $this->put() : $this->post(),
        ]);
    }

    /**
     * Add custom submit button.
     *
     * @param string $attributeName
     * @param mixed $attribute
     * @return $this
     */
    public function addSubmitBtn($attributeName = 'submit', $attribute = [])
    {
        return $this->add(
            $attributeName,
            'submit',
            array_merge($attribute, [
                'attr' => [
                    'name' => $attributeName,
                    'class' => 'btn btn-primary',
                ],
                'label' => $this->updating ? 'Update' : 'Submit',
            ])
        );
    }

    /**
     * Add "Submit & new" button.
     *
     * @param string $attributeName
     * @param mixed $attribute
     * @return $this
     */
    public function addSubmitNewBtn($attributeName = 'submit_new', $attribute = [])
    {
        return $this->addSubmitBtn(
            $attributeName,
            array_merge($attribute, [
                'attr' => [
                    'name' => $attributeName,
                    'class' => 'btn btn-info',
                ],
                'label' => 'Submit & new',
            ])
        );
    }

    /**
     * Add buttons to footer.
     *
     * @return $this
     */
    public function addFooterButtons()
    {
        $self = $this->addSubmitBtn('submit', [
            'label' => $this->updating ? 'Update' : 'Submit',
        ]);

        if (!$this->updating) {
            $self = $self->addSubmitNewBtn();
        }

        return $self->add('reset', 'reset', [
            'attr' => [
                'class' => 'btn btn-default',
            ],
        ]);
    }

    /**
     * Get the post url.
     *
     * @return string
     */
    abstract public function post(): string;

    /**
     * Get the put url.
     *
     * @return string
     */
    abstract public function put(): string;

    /**
     * Get the back url.
     *
     * @return string
     */
    abstract public function back(): string;

    /**
     * Get the content header title
     *
     * @return string
     */
    protected function title()
    {
        $model = $this->updating ? Str::plural(class_basename($this->model)) : '';

        return $this->updating ? Str::title(Str::slug($model), ' ') : '';
    }

    /**
     * Get the content header details
     *
     * @return string
     */
    protected function description()
    {
        $title = Str::lower(Str::singular($this->title()));

        return ($this->updating ? 'Edit ' : 'Add new ') . $title;
    }

    /**
     * Redirects to a destination when form is invalid.
     *
     * @param  null|string $destination The target url.
     * @return HttpResponseException
     */
    public function redirectIfNotValid($destination = null)
    {
        if (! $this->isValid()) {
            $response = redirect($destination);

            if (null === $destination) {
                $response = $response->back();
            }

            flash()->error($this->getErrors());

            $response = $response->withErrors($this->getErrors(), $this->getErrorBag())->withInput();

            throw new HttpResponseException($response);
        }
    }

    /**
     * Get field dynamically.
     *
     * @param string $name
     * @return FormField
     */
    public function __get($name)
    {
        if ('title' === $name || 'description' === $name) {
            return $this->{$name}();
        }

        return parent::__get($name);
    }
}
