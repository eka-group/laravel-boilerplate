<?php

namespace FutureSoft\LaravelBoilerplate;

interface HasTestData
{
    /**
     * Fake data
     *
     * @return array
     */
    public function data(): array;
}
